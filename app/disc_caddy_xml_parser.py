import xml.etree.ElementTree as ET


class Player:

    def __init__(self, disc_caddy_id, name):
        self.disc_caddy_id = int(disc_caddy_id)
        self.name = name


class Hole:

    def __init__(self, par):
        self.par = int(par)


class Course:

    def __init__(self, disc_caddy_id, name, holes):
        self.disc_caddy_id = int(disc_caddy_id)
        self.name = name
        self.holes = holes

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.disc_caddy_id == other.disc_caddy_id
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)


class CourseScore:

    def __init__(self, disc_caddy_id, start_time, player, course):
        self.disc_caddy_id = int(disc_caddy_id)
        self.start_time = start_time
        self.player = player
        self.course = course

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return (self.start_time == other.start_time and
                    self.course == other.course)
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)


class Game:

    def __init__(self, id, start_time, course, players, results):
        self.id = id
        self.start_time = start_time
        self.course = course
        self.players = players
        self.results = results

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return (self.start_time == other.start_time and
                    self.course == other.course)
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)


class DiscCaddyXmlParser:

    def __init__(self, filename):
        self.parse_xml(filename)

    def parse_xml(self, filename):
        tree = ET.parse(filename)
        self.root = tree.getroot()
        self.players = self.parse_players()
        self.games = self.parse_games()

    def parse_players(self):
        players = []
        for player in self.root.findall('player'):
            disc_caddy_id = player.get('id')
            name = player.get('name')
            players.append(Player(disc_caddy_id, name))
        return players

    def parse_games(self):
        games = []
        for score in self.root.findall('course_score'):
            disc_caddy_id = score.get('id')
            start_time = score.get('time_enter')
            player_id = score.get('player_id')
            course_id = score.get('course_id')

            course_score = CourseScore(
                disc_caddy_id, start_time, player_id, course_id)

            course = self.get_course(course_id)

            players = self.get_players_for_game(course_score)

            results = self.get_results_for_game(course_score)

            games.append(
                Game(disc_caddy_id, start_time, course, players, results)
            )

        result = []
        for game in games:
            if game not in result:
                result.append(game)
        return result

    def get_players(self):
        return self.players

    def get_games(self):
        return self.games

    def get_game(self, game_id):
        for score in self.root.findall('course_score'):
            disc_caddy_id = score.get('id')
            if int(disc_caddy_id) == int(game_id):
                start_time = score.get('time_enter')
                player_id = score.get('player_id')
                course_id = score.get('course_id')
                course_score = CourseScore(
                    disc_caddy_id, start_time, player_id, course_id)
                course = self.get_course(course_id)
                players = self.get_players_for_game(course_score)
                results = self.get_results_for_game(course_score)
                return Game(
                    disc_caddy_id, start_time, course, players, results)
        return None

    def get_course(self, course_id):
        for course in self.root.findall('course'):
            disc_caddy_id = course.get('id')
            name = course.get('name')
            if course_id == disc_caddy_id:
                holes = []
                for hole in course.findall('hole'):
                    par = hole.get('par')
                    holes.append(Hole(par))
                return Course(disc_caddy_id, name, holes)
        return None

    def get_players_for_game(self, course_score):
        players = []
        for score in self.root.findall('course_score'):
            disc_caddy_id = score.get('id')
            start_time = score.get('time_enter')
            player_id = score.get('player_id')
            course_id = score.get('course_id')
            course_score2 = CourseScore(
                disc_caddy_id, start_time, player_id, course_id)
            if course_score == course_score2:
                player = self.get_player(player_id)
                players.append(player)
        return players

    def get_results_for_game(self, course_score):
        results = {}
        for score in self.root.findall('course_score'):
            disc_caddy_id = score.get('id')
            start_time = score.get('time_enter')
            player_id = score.get('player_id')
            course_id = score.get('course_id')
            course_score2 = CourseScore(
                disc_caddy_id, start_time, player_id, course_id)
            if course_score == course_score2:
                r = []
                for hole_score in score.findall('hole_score'):
                    r.append(int(hole_score.get('score')))
                results[int(player_id)] = r
        return results

    def get_player(self, player_id):
        for player in self.root.findall('player'):
            disc_caddy_id = player.get('id')
            name = player.get('name')
            p = Player(disc_caddy_id, name)
            if p.disc_caddy_id == int(player_id):
                return p
        return None
