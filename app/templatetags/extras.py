from django.template.defaulttags import register


@register.filter
def get_dict_item(dictionary, key):
    return dictionary.get(key)


@register.filter
def get_list_item(list_, key):
    return list_[key]


@register.filter
def sum_par(holes):
    return sum([hole.par for hole in holes])


@register.filter
def get_sum(dictionary, player_id):
    return sum(get_dict_item(dictionary, player_id))


@register.filter
def get_class(result, par):
    r = result - par
    if r <= -2:
        return 'bg-primary'
    elif r == -1:
        return 'bg-info'
    elif r == 0:
        return 'bg-success'
    elif r == 1:
        return 'bg-warning'
    else:
        return 'bg-danger'
