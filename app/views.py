import os
import tempfile
import uuid
import re

from django.shortcuts import render, redirect

from . import disc_caddy_xml_parser
from .forms import UploadFileForm

PROJECT_DIR = os.path.dirname(os.path.dirname(__file__))
SESSION_KEY_FILE = 'uploadedXml'
TEMP_DIRECTORY = tempfile.gettempdir()
ERROR_FILE_NOT_FOUND = 'Upload a file first'


def home(request):
    return render(request, 'main.html', {'form': UploadFileForm()})


def upload(request):
    if 'uploaded_file' not in request.FILES:
        return redirect('/')
    if SESSION_KEY_FILE in request.session:
        oldfile = request.session[SESSION_KEY_FILE]
        if os.path.isfile(oldfile):
            os.remove(oldfile)
    upload_path = os.path.join(TEMP_DIRECTORY, str(uuid.uuid4()))

    # Write the uploaded file on the disk
    data = request.FILES['uploaded_file'].read()
    with open(upload_path, 'wb+') as destination:
        destination.write(data)

    # Open the file and fix the first line as it has two <?xml ... ?> tags
    # (atleast in the first version of Disc Caddy)
    with open(upload_path, 'r') as f:
        text = f.read()
        pattern = r'[<?xml](.)*?>'
        matches = re.findall(pattern, text)
        if len(matches) > 1:
            text = re.sub(pattern, '', text, count=1)

    # Write the updated file again on the disk
    with open(upload_path, 'w+') as destination:
        destination.write(text)

    request.session[SESSION_KEY_FILE] = upload_path
    return redirect('/games')


def games(request):
    parser = get_parser(request)
    if parser:
        games = parser.get_games()
        return render(request, 'games.html', {'games': games})
    else:
        return render(
            request,
            'main.html',
            {'error': ERROR_FILE_NOT_FOUND, 'form': UploadFileForm}
        )


def players(request):
    parser = get_parser(request)
    if parser:
        players = parser.get_players()
        return render(request, 'players.html', {'players': players})
    else:
        return render(
            request,
            'main.html',
            {'error': ERROR_FILE_NOT_FOUND, 'form': UploadFileForm}
        )


def game(request, game_id):
    parser = get_parser(request)
    if parser:
        game = parser.get_game(game_id)
        return render(request, 'game.html', {'game': game})
    else:
        return render(
            request,
            'main.html',
            {'error': ERROR_FILE_NOT_FOUND, 'form': UploadFileForm}
        )


def get_parser(request):
    if SESSION_KEY_FILE in request.session:
        xml = request.session[SESSION_KEY_FILE]
        if os.path.isfile(xml):
            print('Reading XML from {}'.format(xml))
            return disc_caddy_xml_parser.DiscCaddyXmlParser(xml)
    return None
