from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns(
    '',
    url(r'^$', 'app.views.home', name='home'),
    url(r'^upload/$', 'app.views.upload', name='upload'),
    url(r'^games/$', 'app.views.games', name='games'),
    url(r'^players/$', 'app.views.players', name='players'),
    url(r'^game/(\d+)$', 'app.views.game', name='game'),
)
